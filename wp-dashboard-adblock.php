<?php
/*
Plugin Name: WordPress Dashboard AdBlock
Description: Hide ads and nags from WordPress plugins.
Version: 0.1
Author: Mark Branly, Registered Creative
Author URI: http://registeredcreative.com/
*/

namespace RC\WPDashboardAdBlock;

function wp_dashboard_adblock(){
    $css_files = plugin_dir_path(__FILE__ ) . 'css/*.css';

    $css_output = '';
    foreach (glob($css_files) as $file) {
        // If this output starts getting huge, we could check to
        //  see if the plugin is active before including it's css.
        $css_output .= file_get_contents($file);
    }

    echo sprintf('<style>%s</style>', namespace\minify_css($css_output));
};
add_action('admin_head', __NAMESPACE__ . '\\wp_dashboard_adblock', 999);

// CSS Minify taken from:
// http://stackoverflow.com/questions/15195750/minify-compress-css-with-regex
function minify_css($str){
    # remove comments first (simplifies the other regex)
    $re1 = <<<'EOS'
(?sx)
  # quotes
  (
    "(?:[^"\\]++|\\.)*+"
  | '(?:[^'\\]++|\\.)*+'
  )
|
  # comments
  /\* (?> .*? \*/ )
EOS;

    $re2 = <<<'EOS'
(?six)
  # quotes
  (
    "(?:[^"\\]++|\\.)*+"
  | '(?:[^'\\]++|\\.)*+'
  )
|
  # ; before } (and the spaces after it while we're here)
  \s*+ ; \s*+ ( } ) \s*+
|
  # all spaces around meta chars/operators
  \s*+ ( [*$~^|]?+= | [{};,>~+-] | !important\b ) \s*+
|
  # spaces right of ( [ :
  ( [[(:] ) \s++
|
  # spaces left of ) ]
  \s++ ( [])] )
|
  # spaces left (and right) of :
  \s++ ( : ) \s*+
  # but not in selectors: not followed by a {
  (?!
    (?>
      [^{}"']++
    | "(?:[^"\\]++|\\.)*+"
    | '(?:[^'\\]++|\\.)*+'
    )*+
    {
  )
|
  # spaces at beginning/end of string
  ^ \s++ | \s++ \z
|
  # double spaces to single
  (\s)\s+
EOS;

    $str = preg_replace("%$re1%", '$1', $str);
    return preg_replace("%$re2%", '$1$2$3$4$5$6$7', $str);
}